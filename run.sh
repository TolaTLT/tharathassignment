#!/bin/bash
set -e

# Change to the project directory

cd /home/ubuntu/tharathassignment/

#sudo chmod 777 

# Pull the latest changes from the Git repository
echo "git pull...dd"
sudo git pull origin main  # Replace 'master' with your branch if it's different

echo "restart servce...ddd"
sudo systemctl restart lotr.service

# Install/update project dependencies using npm
sudo npm install --production

# Perform any additional build or update tasks here
sudo pm2 restart server

# Display a message indicating that the update is complete
echo "Node.js project updated and running in production mode!"
